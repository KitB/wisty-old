import web
import logging

from google.appengine.api import users
from google.appengine.ext import db

def user_exists(user):
    try:
        getUser(user)
        return True
    except IndexError:
        return False

def getUser(user):
    q = User.all()
    q.filter("user =", user)
    return q[0]

def ensureUser(user):
    isUser = user_exists(user)
    if not isUser:
        raise web.seeother('/signup?continue=%s' % web.ctx.get('path', '/'))

def ensureLogin():
    user = users.get_current_user()
    if user:
        ensureUser(user)
        return user
    else:
        raise web.seeother(getLoginURL())

def ensureAdmin():
    user = ensureLogin()
    if users.is_current_user_admin():
        return user
    else:
        raise web.seeother("/")

def getLoginURL():
    return users.create_login_url(web.ctx.get('path', '/'))

def getLogoutURL():
    return users.create_logout_url(web.ctx.get('path', '/'))

def getUserURL(user=None):
    if user is None:
        user = getCurrentUser()
    return '/u/%s' % user.key().name()

def getCurrentUser():
    user = users.get_current_user()
    return getUser(user)

def loggedIn():
    u = users.get_current_user()
    out = users.get_current_user() is not None and user_exists(u)
    return out

class User(db.Model):
    user = db.UserProperty()
