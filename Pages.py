import web
import logging

from Wishlist import Wishlist, Wish
from User import User, ensureLogin, getLogoutURL, getUserURL, getCurrentUser, loggedIn
from Form import Form, CombinedForm

from google.appengine.api import users
from google.appengine.ext import db


# Be careful filling up the globals in the following line; it reduces separation of concern
render = web.template.render('templates/', base='base', globals={'logoutURL':getLogoutURL, 'currentUser':getCurrentUser,'userURL':getUserURL, 'loggedIn':loggedIn})

def see_self():
    web.seeother(web.ctx.get('path', '/'))

class Page(object):
    def __init__(self):
        try:
            if self.require_login:
                ensureLogin()
        except AttributeError:
            pass

class Index(Page):
    require_login = True
    def GET(self):
        raise web.seeother(getUserURL())

class UserPage(Page):
    def GET(self, user):
        u = User.get_by_key_name(user)
        return render.user(u)

class WishlistPage(Page):
    form = Form( web.form.Textbox(name='object_name', description='Name of wish', size='50')
               , web.form.Textbox(name='href', description='Link to wish', size='50')
               , web.form.Textbox(name='priority', description='Wish priority')
               , web.form.Button('Add wish to list')
               )
    require_login = True
    def GET(self, user, wishlist):
        f = self.form()
        form = CombinedForm([f], method="POST")
        return render.wishlist(Wishlist.get(wishlist), form)

    def POST(self, user, wishlist):
        form = self.form()
        if not form.validates():
            return "Form didn't validate"
        w = Wish( object_name=form.d.object_name
                , href=form.d.href
                , priority=int(form.d.priority)
                , wishlist=db.Key(wishlist)
                )
        w.put()
        see_self()

class NewList(Page):
    form = Form( web.form.Textbox(name='longname', description='List description', size='50')
               , web.form.Button('Make new list')
               )
    require_login = True
    def GET(self, error=None):
        f = self.form()
        form = CombinedForm([f], method="POST")
        return render.newlist(form, error)

    def POST(self):
        form = self.form()
        if not form.validates():
            return self.GET("Form didn't validate")
        w = Wishlist(longname=form.d.longname, user=getCurrentUser())
        w.put()
        raise web.seeother('%s/%s' % (getUserURL(), w.key()))

class Signup(Page):
    form = Form( web.form.Textbox(name='username', description='Desired user name', size='30')
               , web.form.Hidden(name='continue')
               , web.form.Button("Sign up")
               )
    def GET(self):
        returnAddr = web.input(**{'continue':'/'})['continue']
        f = self.form()
        f.fill(**{'continue':returnAddr})
        form = CombinedForm([f], method="POST")
        return render.signup(form)

    def POST(self):
        form = self.form()
        if not form.validates():
            return "Form didn't validate"
        logging.info("Creating user %s" % form.d.username)
        new_user = User(user=users.get_current_user(), key_name=form.d.username)
        new_user.put()
        raise web.seeother(form.d['continue'])
