from google.appengine.ext import db
from User import User

class Wishlist(db.Model):
    longname = db.StringProperty()
    user = db.ReferenceProperty(User)

class Wish(db.Model):
    object_name = db.StringProperty()
    href = db.TextProperty()
    #price = db.IntegerProperty()
    #bought = db.BooleanProperty()
    priority = db.IntegerProperty()
    wishlist = db.ReferenceProperty(Wishlist)
