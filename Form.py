from web.form import Form as BaseForm
from web import utils, net

class Form(BaseForm):
    def render(self):
        out = ''
        for i in self.inputs:
            html = utils.safeunicode(i.pre) + i.render() + self.rendernote(i.note) + utils.safeunicode(i.post)
            if i.is_hidden():
                out += '<p>%s</p>\n' % html
            else:
                out += '<p><label for="%s">%s</label>%s</p>\n' % (i.id, net.websafe(i.description), html)
        return out

def ensureIsForm(form):
    if not issubclass(form.__class__, BaseForm):
        raise TypeError("Combined Form needs web.py form or derivative")


class CombinedForm(object):
    def __init__(self, forms=[], **attrs):
        for form in forms:
            ensureIsForm(form)
        self._forms = forms
        self.attrs = attrs

    def addForm(self, form):
        ensureIsForm(form)
        self._forms.append(form)

    def render(self, withFormTags=True):
        html = ''
        for form in self._forms:
            html += form.render()
        out = ("<form%s>\n%s\n</form>\n" % (self.renderAttrs(), html)) if withFormTags else html
        return out

    def renderAttrs(self):
        out = ''
        for attrName, attrVal in self.attrs.items():
            out += ' %s="%s"' % (attrName, attrVal)
        return out
